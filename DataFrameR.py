import pandas as pd


def tocontainer(func):
    def wrapper(*args, **kwargs):
        args = list(args)
        # E.g. can't join DataFrameR with pandas, so cast to a panda df
        args = [newarg.to_pandas() if
                type(newarg) is DataFrameR else newarg for newarg in args]
        result = func(*args, **kwargs)
        return DataFrameR(result)
    return wrapper


class DataFrameR(object):
    def __init__(self, df):
        self.contained = df

    def __getitem__(self, item):
        result = self.contained[item]
        if isinstance(result, type(self.contained)):
            result = DataFrameR(result)
        return result

    def __getattr__(self, item):

        if item in ['select', 'filter', 'distinct']:
            print("over riding ")
            result = self.getattr(self, item)
        else:
            result = getattr(self.contained, item)

        if callable(result):
            result = tocontainer(result)
        return result

    def __repr__(self):
        return self.contained.to_string()

    def select(self, *args, **kwargs):
        return tocontainer(self.contained.filter)(*args, **kwargs)

    def filter(self, col_filt):
        if self.__is_lambda(col_filt):
            return DataFrameR(self.contained[col_filt(self.contained)])
        else:
            return DataFrameR(self.contained[col_filt])

    def mutate(self, *args, **kwargs):
        return tocontainer(self.contained.assign)(*args, **kwargs)

    def distinct(self, *args, **kwargs):
        return tocontainer(self.contained.drop_duplicates)(*args, **kwargs)

    def __is_lambda(self, v):
        LAMBDA = lambda: 0
        return isinstance(v, type(LAMBDA)) and v.__name__ == LAMBDA.__name__

    def to_pandas(self):
        return self.contained

    def summary(self):
        print(self.contained.describe(include="all"))

    def structure(self):
        # https://stackoverflow.com/questions/27637281/what-are-python-pandas-equivalents-for-r-functions-like-str-summary-and-he
        print(self.contained.info())
        print()
        print(self.contained.shape)
        print()
        print(self.contained.apply(lambda x: [x.unique()]))

