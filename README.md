# R-like Syntax for Pandas

This is a simple wrapper to demonstrate that you can easily replicate R dplyr syntax with Pandas.  

```
df = DataFrameR(pd.DataFrame({"hello": [1, 2, 3, 2], "world": [2, 3, 1, 3]}))

df \
    .select(["hello"]) \
    .distinct() \
    .filter(df.hello > 2)

df.distinct(["hello", "world"])
```

```
# using lambdas
df.filter(lambda x: x.world > x.hello)

df.filter(lambda x: x.world > x.hello).groupby("hello").count()

type(df.filter(lambda x: x.world > x.hello))
type(df.to_pandas())
```


## Joins

```
df1 = DataFrameR(pd.DataFrame({"hello": [1, 2, 3, 2], "world": [2, 3, 1, 3]}))
df2 = DataFrameR(pd.DataFrame({"hello": [1, 2, 3, 2], "newyork": [2, 3, 1, 3]}))

df1.merge(df2, on="hello")
type(df1.merge(df2, on="hello"))


df1.to_pandas()
```

## Filtering on Dates
```
import datetime
col1 = "dt"
df_dt = DataFrameR(pd.DataFrame({col1: [pd.datetime(2010, 2, 4),
                                        pd.datetime(2012, 3, 5),
                                        pd.datetime(2015, 1, 1)],
                                 'apples': [2, 3, None],
                                 'pears': [4, None, None],
                                 'categorical': ['a','b','a']}))

df2 = df_dt \
    .filter(lambda x: x[col1] > pd.to_datetime("2011-01-01")) \
    .assign(dt_plus=lambda x: x[col1] + pd.DateOffset(180)) \
    .assign(dt_plus_again=lambda x: x.dt_plus + datetime.timedelta(hours=3)) \
    .filter(lambda x: x.dt_plus_again < pd.datetime(2015, 8, 3, 4, 0, 0))
```


## dplyr 'mutate' syntax
```
df3 = df_dt \
    .filter(lambda x: x[col1] > pd.to_datetime("2011-01-01")) \
    .mutate(dt_plus=lambda x: x[col1] + pd.DateOffset(180)) \
    .mutate(dt_plus_again=lambda x: x.dt_plus + datetime.timedelta(hours=3)) \
    .filter(lambda x: x.dt_plus_again < pd.datetime(2015, 8, 3, 4, 0, 0))
```


## Summary

A summary function similar to R
```

from sklearn.datasets import load_iris
data = load_iris()
iris_df = pd.DataFrame(data.data, columns=data.feature_names)
iris_df.head()


DataFrameR(iris_df).summary()
```


